# ESLINT Beispiel Projekt

Zum Prüfen:

```bash
npm run lint
```


Zum Prüfen & automatischen Beheben von Fehlern:

```bash
npm run lint:fix
```